package CountingPairs;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;



class Result {

    /*
     * Complete the 'countPairs' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. INTEGER_ARRAY arr
     *  2. LONG_INTEGER k
     */

    public static int countPairs(List<Integer> arr, long k) {
    // Write your code here

        Map<Integer, Integer> pairs = new HashMap<>();
        
        arr.sort(null);
        while(arr.size() > 1 && arr.get(0) < k/2){
            if(pairs.containsKey(arr.get(0)) || pairs.containsKey((int) (k - (long) arr.get(0)))){
                arr.remove(0);
                continue;
            }
            
            int j = Collections.binarySearch(arr, (int) (k - arr.get(0)));

            if(j <= 0)
                arr.remove(0);

            else{
                pairs.put(arr.get(0), arr.get(j));
                arr.remove(0);
                arr.remove(j);
            }
        }
        return pairs.size();       
    }
}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File("result.txt")));

        int arrCount = Integer.parseInt(bufferedReader.readLine().trim());

        List<Integer> arr = new ArrayList<>();

        for (int i = 0; i < arrCount; i++) {
            int arrItem = Integer.parseInt(bufferedReader.readLine().trim());
            arr.add(arrItem);
        }

        long k = Long.parseLong(bufferedReader.readLine().trim());

        int result = Result.countPairs(arr, k);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}